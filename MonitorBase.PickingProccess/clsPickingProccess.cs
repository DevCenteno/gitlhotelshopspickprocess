﻿using Epicor.ServiceModel.StandardBindings;
using Erp.BO;
using Erp.Proxy.BO;
using MonitorBase.Dll;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace MonitorBase.PickingProccess
{
 public  class clsPickingProccess
    {
        public int SubJobID;
        public SqlConnection con;
        public GlobalMembers GlbGM;

        string appServerUrl;
        string Username;
        string Password;

        public DataTable TablePick;
        /// <summary>
        /// método validar del monitor 
        /// </summary>
        /// <returns></returns>
        public bool StartValidation()
        {
            bool result = false;
        SqlCommand command = new SqlCommand("[Int_StartValidation]", con);
        command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);

            DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(dt);
            result = Convert.ToBoolean(dt.Rows[0]["Start"].ToString());
            return result;
        }

    /// <summary>
    /// Método para obtener datos de la bd 
    /// </summary>
    /// <returns></returns>
    public bool GetDatos()
    {
        ClaseGetDatos classGet = new ClaseGetDatos();
        classGet.SubJobID = SubJobID;
        bool result = false;
        string error = "";
        TablePick = classGet.GetDatable(con, "Int_GetDatosPicking", "getpicking", out result, out error);
        if (result == false) { return false; }
        return true;
    }

    public GlobalMembers Execute(GlobalMembers globalMembers)
    {
        GlbGM = globalMembers;

        GlbGM.MessageStatus = true;
        SubJobID = GlbGM.SubJobId;
        con = GlbGM.getConexionSql();

        appServerUrl = GlbGM.getConexionEpicor().Servidor;
        Username = GlbGM.getConexionEpicor().User;
        Password = GlbGM.getConexionEpicor().Password;

        if (!StartValidation())
        {
            GlbGM.MessageStatus = false;
            GlbGM.MessageError = "No inicio, por StartValidation";
            return GlbGM;

        }
        else
        {
            SaveStatus("Running");

            if (GetDatos())
            {
                Process();
            }
            else
            {
                GlbGM.MessageStatus = false;
                GlbGM.MessageError = "Error obteniendo datos";
                return GlbGM;
            }

            return globalMembers;
        }
    }

    public void Process()
    {
        try
        {
                DataTable datablet = TablePick;
                DataRow dt = datablet.Rows[0];
                string Company = dt["Company"].ToString();
                string Plant = dt["Plant"].ToString();


            CustomBinding wcfBindingU = NetTcp.UsernameWindowsChannel();
            Uri uriUserFile = new Uri(String.Format(@"{0}/Ice/BO/{1}.svc", appServerUrl, "UserFile"));
            Ice.Proxy.BO.UserFileImpl oUserFile = new Ice.Proxy.BO.UserFileImpl(wcfBindingU, uriUserFile);
            oUserFile.ClientCredentials.UserName.UserName = Username;
            oUserFile.ClientCredentials.UserName.Password = Password;
            //ruta de acceso para entrar directo en una compañia y la planta 
            oUserFile.SaveSettings(Username, true, Company, true, false, true, true, true, true, true, true, true,
                                   false, false, -2, 0, 1456, 886, 2, "MAINMENU", Plant, "", 0, -1, 0, "", false, "Default");

            CustomBinding wcfBinding = NetTcp.UsernameWindowsChannel();
            string partName = "IssueReturn";
            Uri partUri = new Uri(String.Format(@"{0}/Erp/BO/{1}.svc", appServerUrl, partName));

                using (var IReturnImpl = new IssueReturnImpl(wcfBinding, partUri))
                {
                    IReturnImpl.ClientCredentials.UserName.UserName = Username;
                    IReturnImpl.ClientCredentials.UserName.Password = Password;

                    string SysRowID = dt["SysRowID"].ToString();
                    int QtyToMove = Convert.ToInt32(dt["Quantity"].ToString());
                    String pcAction = "";
                    String pcMessage = "";
                    decimal pdQtyAvailable = 0;
                    Guid SysRowId_guid = new Guid(SysRowID);
                    IReturnImpl.PreGetNewIssueReturn(SysRowId_guid, out pcAction, out pcMessage, out pdQtyAvailable);

                    IssueReturnDataSet ds = new IssueReturnDataSet();
                    string pcTranType = "";
                    string pCallProcess = "";
                    IReturnImpl.GetNewIssueReturn(pcTranType, SysRowId_guid,pCallProcess, ds);
                    // validar si 
                    string QtyRequired_str = ds.Tables["IssueReturn"].Rows[0]["TranQty"].ToString();
                    decimal QtyRequired_dec = Convert.ToDecimal(QtyRequired_str);
                    int QtyRequired = Convert.ToInt32(QtyRequired_dec);


                    if (QtyToMove > QtyRequired)
                    {
                        throw new System.ArgumentException("La cantidad que reuqiere mover es mayor a la requerida");
                    }

                    IReturnImpl.OnChangeTranQty(QtyToMove, ds);
                   

                    string mltName = "MaterialQueue";
                    Uri mltSvcUri = new Uri(String.Format(@"{0}/Erp/BO/{1}.svc", appServerUrl, mltName));
                    using (var Materialimpl = new MaterialQueueImpl(wcfBinding, mltSvcUri))
                    {
                        MoveRequestDataSet dsMtl = new MoveRequestDataSet();
                        Materialimpl.ClientCredentials.UserName.UserName = Username;
                        Materialimpl.ClientCredentials.UserName.Password = Password;

                        Boolean existe = Materialimpl.VerifyMtlQueueExists(SysRowId_guid);

                        if (existe == true)
                        {
                            Boolean requiresUserInput;
                            IReturnImpl.PrePerformMaterialMovement(ds, out requiresUserInput);

                            String pcNeqQtyAction;
                            String pcNeqQtyMessage;
                            String pcPCBinAction;
                            String pcPCBinMessage;
                            String pcOutBinAction;
                            String pcOutBinMessage;
                            IReturnImpl.MasterInventoryBinTests(ds,out pcNeqQtyAction, out pcNeqQtyMessage, out pcPCBinAction,
                                       out pcPCBinMessage, out pcOutBinAction, out pcOutBinMessage);

                            Boolean plNegQtyAction = false;
                            String legalNumberMessage = "";
                            String partTranPKs = "";
                            IReturnImpl.PerformMaterialMovement(plNegQtyAction,ds,out legalNumberMessage,out partTranPKs);                                                       
                            SaveStatus("Finished");
                            SaveEstado("Int_GetDatosPicking", "procesado");
                            GlbGM.MessageStatus = true;
                            GlbGM.MessageError = "";
                        }
                    } 
                }
            }


        catch (Exception ex)
        {
            SaveEstado("Int_GetDatosPicking", "error");
            SaveStatus("Error");
            GlbGM.MessageStatus = false;
            GlbGM.MessageError = "Process: " + ex.ToString();
            SaveError(ex.ToString());
        }
    }
   
    public void SaveStatus(string Status)
    {
        SqlCommand command = new SqlCommand("[Int_SaveStatus]", con);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@SubJobID", SubJobID);
        command.Parameters.AddWithValue("@Status", Status);

        DataTable ds = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(ds);
    }
    public void SaveEstado(string procedure, string Tipo)
    {
            SqlCommand command = new SqlCommand(procedure, con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);
            command.Parameters.AddWithValue("@Tipo", Tipo);

            DataTable ds = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
      
      }       
        public void SaveError(string MsgError)
    {
        SqlCommand command = new SqlCommand("[Int_SaveError]", con);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@SubJobID", SubJobID);
        command.Parameters.AddWithValue("@MsgError", MsgError);

        DataTable ds = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(ds);
    }
    public void SaveReference(string Reference, string Table)
    {
        SqlCommand command = new SqlCommand("[Int_SaveReference]", con);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddWithValue("@SubJobID", SubJobID);
        command.Parameters.AddWithValue("@Reference", Reference);
        command.Parameters.AddWithValue("@Table", Table);

        DataTable ds = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        adapter.Fill(ds);
    }
}


public class ClaseGetDatos
{
    public int SubJobID { get; set; }
    public DataTable GetDatable(SqlConnection Con, string procedure, string Tipo, out bool result, out string Error)
    {
        DataTable ds = new DataTable();
        try
        {
            SqlConnection connection = Con;
            SqlCommand command = new SqlCommand(procedure, connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SubJobID", SubJobID);
            command.Parameters.AddWithValue("@Tipo", Tipo);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);

            result = true;
            Error = "";
            return ds;

        }
        catch (Exception ex)
        {
            result = false;
            Error = ex.Message.ToString();
            return ds;
        }

    }

    public DataTable GetDatableString(SqlConnection Con, string StringSQL, out bool result, out string Error)
    {
        DataTable ds = new DataTable();
        try
        {
            SqlConnection connection = Con;
            SqlCommand command = new SqlCommand(StringSQL, connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);

            result = true;
            Error = "";
            return ds;

        }
        catch (Exception ex)
        {
            result = false;
            Error = ex.Message.ToString();
            return ds;
        }

    }

}
}